# Elevation

![](africa_SRTM.jpg)

## Selected Datasets

| Name                                             | Description              | Source  | Type   | Format    | License | Size   |
| ------------------------------------------------ | ------------------------ | ------- | ------ | --------- | ------- | ------ |
| [SRTM](https://nextcloud.jailbreak.paris/s/SRTM) | SRTM-based contour lines for the Cameroon area | [OpenDEM](https://opendem.info/download_contours.html) (see below) | Vector | Shapefile | ODbL    | 749 MB |


## OpenDEM

[OpenDEM](https://opendem.info/) is an open data project (similar to OSM) for collecting and improving a free digital elevation model of the earth. It was started in 2011 by Martin Over and currently contains [Shuttle Radar Topography Mission](https://wiki.openstreetmap.org/wiki/SRTM) (SRTM) height relief and [GEBCO](https://www.gebco.net/) bathymetry data set.

### OpenDEM Sources

- SRTM data by the [USGS](https://dds.cr.usgs.gov/srtm/version2_1)
- Enhanced datasets for mountain areas by [Panorama Viewfinders](https://www.viewfinderpanoramas.org/), Stand: June 2012
- [Radarsat Antarctic Mapping Project](https://nsidc.org/data/nsidc-0082.html) (Antarctica)
- [National Elevation Dataset](https://ned.usgs.gov/) (NED) - Alaska
- [Canadian Digital Elevation Dataset](https://www.geobase.ca/geobase/en/index.html) (CDED) - > 60° N


**Ask to the facilittators to get the datasets. all are available on hard drive**